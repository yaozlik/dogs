package com.osmi.dogs.framework.api

import com.osmi.dogs.domain.Dog
import retrofit2.Response
import retrofit2.http.GET

interface DogService {
    @GET("api/880188946124021760")
    suspend fun getDogs(): Response<List<Dog>>
}
package com.osmi.dogs.presentation.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.osmi.dogs.R
import com.osmi.dogs.domain.Dog
import com.squareup.picasso.Picasso

class DogAdapter : RecyclerView.Adapter<DogAdapter.ViewHolder>() {

    var dogs = ArrayList<Dog>()

    fun addDogs(dogs: ArrayList<Dog>) {
        this.dogs =dogs
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dogs[position]
        holder.bind(item)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.dog_item, parent, false))
    }
    override fun getItemCount(): Int {
        return dogs.size
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val dogName = view.findViewById(R.id.dogNameTextView) as TextView
        private val dogDescription = view.findViewById(R.id.dogDescriptionTextView) as TextView
        private val dogAge = view.findViewById(R.id.dogAgeTextView) as TextView
        private val dogImage = view.findViewById(R.id.dogAvatarImageView) as ImageView

        fun bind(dog: Dog){
            dogName.text = dog.dogName
            dogDescription.text = dog.description
            dogAge.text = dog.age.toString()
            dogImage.loadUrl(dog.image)
        }

        private fun ImageView.loadUrl(url: String) {
            Picasso.get()
                .load(url)
                .fit()
                .centerCrop()
                .into(this)
        }
    }
}
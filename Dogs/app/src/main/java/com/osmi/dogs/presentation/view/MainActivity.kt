package com.osmi.dogs.presentation.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.osmi.dogs.R
import com.osmi.dogs.domain.Dog
import com.osmi.dogs.presentation.view.adapter.DogAdapter
import com.osmi.dogs.presentation.viewmodel.DogViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var recycler : RecyclerView
    private var adapter = DogAdapter()
    private val viewModel: DogViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        addObservers()
        viewModel.getDogs()
    }

    private fun initViews() {
        recycler = findViewById(R.id.dogsRV)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter
    }

    private fun addObservers() {
        viewModel.dogsData.observe(this, {
            adapter.addDogs(it as ArrayList<Dog>)
        })
    }
}
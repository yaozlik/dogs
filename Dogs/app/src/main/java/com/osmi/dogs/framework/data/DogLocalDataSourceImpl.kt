package com.osmi.dogs.framework.data

import com.osmi.dogs.data.datasource.local.DogLocalDataSource
import com.osmi.dogs.domain.Dog
import com.osmi.dogs.domain.toDogEntities
import com.osmi.dogs.framework.database.DogDatabase
import com.osmi.dogs.framework.database.entity.toDogs
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DogLocalDataSourceImpl @Inject constructor(private val database: DogDatabase): DogLocalDataSource {
    override suspend fun getDogs(): List<Dog> {
        return database.dogDao().getAll().toDogs()
    }

    override suspend fun insertDogs(dogs: List<Dog>) {
        database.dogDao().insert(dogs.toDogEntities())
    }

}
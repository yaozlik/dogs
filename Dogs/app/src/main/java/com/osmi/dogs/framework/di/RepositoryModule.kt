package com.osmi.dogs.framework.di

import com.osmi.dogs.data.datasource.local.DogLocalDataSource
import com.osmi.dogs.data.datasource.remote.DogRemoteDataSource
import com.osmi.dogs.data.repository.DogRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    fun provideDogRepository(
        remoteDataSource: DogRemoteDataSource,
        localDataSource: DogLocalDataSource) = DogRepository(remoteDataSource, localDataSource)
}
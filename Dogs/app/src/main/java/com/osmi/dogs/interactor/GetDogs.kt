package com.osmi.dogs.interactor

import com.osmi.dogs.data.repository.DogRepository
import com.osmi.dogs.domain.Dog
import javax.inject.Inject

class GetDogs @Inject constructor(private val repository: DogRepository) {
    suspend fun invoke(): List<Dog> = repository.getDogs()
}
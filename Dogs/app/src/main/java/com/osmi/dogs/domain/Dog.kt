package com.osmi.dogs.domain

import com.osmi.dogs.framework.database.entity.DogEntity

data class Dog(val dogName: String,
               val description: String,
               val age: Int,
               val image: String)

fun Dog.toDogEntity(): DogEntity {
    return DogEntity(this.dogName, this.description, this.age, this.image)
}

fun List<Dog>.toDogEntities(): List<DogEntity> {
    val entities = ArrayList<DogEntity>()

    this.forEach {
        entities.add(it.toDogEntity())
    }

    return entities
}

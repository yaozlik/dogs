package com.osmi.dogs.framework.data

import com.osmi.dogs.data.datasource.remote.DogRemoteDataSource
import com.osmi.dogs.domain.Dog
import com.osmi.dogs.framework.api.DogService
import com.osmi.dogs.framework.api.RetrofitHelper
import javax.inject.Singleton

@Singleton
class DogRemoteDataSourceImpl: DogRemoteDataSource {

    private val retrofit = RetrofitHelper.getRetrofit()

    override suspend fun getDogs(): List<Dog> {
        val call = retrofit.create(DogService::class.java).getDogs()

        return call.body() ?: emptyList()
    }
}
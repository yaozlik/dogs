package com.osmi.dogs.framework.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.osmi.dogs.domain.Dog

@Entity
data class DogEntity(
    @PrimaryKey val dogName: String,
    val description: String,
    val age: Int,
    val image: String)

fun DogEntity.toDog(): Dog {
    return Dog(this.dogName, this.description, this.age, this.image)
}

fun List<DogEntity>.toDogs(): List<Dog> {
    val dogs = ArrayList<Dog>()

    this.forEach {
        dogs.add(it.toDog())
    }

    return dogs
}

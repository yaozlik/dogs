package com.osmi.dogs.framework.di

import com.osmi.dogs.data.repository.DogRepository
import com.osmi.dogs.interactor.GetDogs
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class InteractorModule {
    @Provides
    fun provideGetDogsInteractor(repository: DogRepository) = GetDogs(repository)
}
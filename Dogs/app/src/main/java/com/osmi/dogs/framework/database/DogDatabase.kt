package com.osmi.dogs.framework.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.osmi.dogs.framework.database.dao.DogDao
import com.osmi.dogs.framework.database.entity.DogEntity

@Database(
    entities = [DogEntity::class],
    version = 1
)
abstract class DogDatabase: RoomDatabase() {
    abstract fun dogDao(): DogDao
}
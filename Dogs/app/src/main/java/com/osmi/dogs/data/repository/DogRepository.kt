package com.osmi.dogs.data.repository

import com.osmi.dogs.data.datasource.local.DogLocalDataSource
import com.osmi.dogs.data.datasource.remote.DogRemoteDataSource
import com.osmi.dogs.domain.Dog
import javax.inject.Inject

class DogRepository @Inject constructor(
    private val remoteDataSource: DogRemoteDataSource,
    private val localDataSource: DogLocalDataSource) {

    suspend fun getDogs(): List<Dog> {
        val dogs = localDataSource.getDogs()

        return if (dogs.isEmpty()) {
            val dogsRemote = remoteDataSource.getDogs()
            localDataSource.insertDogs(dogsRemote)
            dogsRemote
        } else {
            dogs
        }
    }
}
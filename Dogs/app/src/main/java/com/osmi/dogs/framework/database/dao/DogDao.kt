package com.osmi.dogs.framework.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.osmi.dogs.framework.database.entity.DogEntity

@Dao
interface DogDao {

    @Query("SELECT * from DogEntity")
    suspend fun getAll(): List<DogEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(dogs: List<DogEntity>)
}
package com.osmi.dogs.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.osmi.dogs.domain.Dog
import com.osmi.dogs.interactor.GetDogs
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DogViewModel @Inject constructor(private val getDogs: GetDogs): ViewModel() {
    val dogsData = MutableLiveData<List<Dog>>()

    fun getDogs() {
        viewModelScope.launch {
            dogsData.postValue(getDogs.invoke())
        }
    }
}
package com.osmi.dogs.framework.di

import com.osmi.dogs.data.datasource.local.DogLocalDataSource
import com.osmi.dogs.data.datasource.remote.DogRemoteDataSource
import com.osmi.dogs.data.repository.DogRepository
import com.osmi.dogs.framework.data.DogLocalDataSourceImpl
import com.osmi.dogs.framework.data.DogRemoteDataSourceImpl
import com.osmi.dogs.framework.database.DogDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DataSourceModule {

    @Provides
    fun provideDogLocalDataSource(database: DogDatabase):DogLocalDataSource = DogLocalDataSourceImpl(database)

    @Provides
    fun provideDogRemoteDataSource():DogRemoteDataSource = DogRemoteDataSourceImpl()
}
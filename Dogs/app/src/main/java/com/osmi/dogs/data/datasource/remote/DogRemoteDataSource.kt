package com.osmi.dogs.data.datasource.remote

import com.osmi.dogs.domain.Dog

interface DogRemoteDataSource {
    suspend fun getDogs(): List<Dog>
}
package com.osmi.dogs.data.datasource.local

import com.osmi.dogs.domain.Dog

interface DogLocalDataSource {
    suspend fun getDogs(): List<Dog>
    suspend fun insertDogs(dogs: List<Dog>)
}